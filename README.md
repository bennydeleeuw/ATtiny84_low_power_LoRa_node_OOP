# ATtiny84 Low Power LoRa Node
This code was written and build with the PlatformIO tools.
Running on Mac OS and using the Atom editor.

## 2020-08-15 changes
- Added this README File
- the code can be compiled (again) with the latest Arduino core
- due to changes in the ```framework-arduino-avr-attiny/cores/tiny/Arduino.h```
the pin numbering was not correct anymore. Added definitions for the original pin numbering of the ATtiny84 chip.

